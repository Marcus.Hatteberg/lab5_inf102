package INF102.lab5.graph;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

import javax.management.Query;

public class AdjacencySet<V> implements IGraph<V> {

    HashMap<V, Set<V>> graph = new HashMap<>();
    Set<V> nodes = new HashSet<>();

    @Override
    public int size() {
        return this.nodes.size();
    }

    @Override
    public void addNode(V node) {
        this.nodes.add(node);
        graph.put(node, new HashSet<V>());
    }

    @Override
    public void removeNode(V node) {
        this.nodes.remove(node);
        graph.remove(node);
    }

    @Override
    public void addEdge(V u, V v) {
        graph.get(u).add(v);
        graph.get(v).add(u);
    }

    @Override
    public void removeEdge(V u, V v) {
        graph.get(u).remove(v);
        graph.get(v).remove(u);
    }

    @Override
    public boolean hasNode(V node) {
        return graph.containsValue(node);
    }

    @Override
    public boolean hasEdge(V u, V v) {
        Set<V> u_neighbours = getNeighbourhood(u);
        Set<V> v_neighbours = getNeighbourhood(v);
        if(u_neighbours.contains(v) && v_neighbours.contains(u))
            return true;
        return false;
    }

    @Override
    public Set<V> getNeighbourhood(V node) {
        return graph.get(node);
    }

    //Inspiration taken from:
    //https://git.app.uib.no/ii/inf102/22h/students/lecture_code_inf102_h22/-/blob/main/src/main/java/lecture11/Graph.java

    public void addNeigbours(V start, Queue<V> search_list) {
        for(V neighbour : getNeighbourhood(start)){
            search_list.offer(neighbour);
        }
    }

    @Override
    public boolean connected(V u, V v) {
        HashSet<V> found = new HashSet<>();
        Queue<V> search_list = new LinkedList<>();

        found.add(u);
        addNeigbours(u, search_list);

        while(!search_list.isEmpty()){
            V next = search_list.poll();
            if(found.contains(next)){
                continue;
            }
            found.add(next);
            addNeigbours(next, search_list);
        }

        return found.contains(u) && found.contains(v);
    }

}
